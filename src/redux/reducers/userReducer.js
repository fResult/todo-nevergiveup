const FETCH_USER = 'FETCH_USER'

const initUserState = {}

const userReducer = (state = initUserState, action) => {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        fetchUser: action.user,
      }
    default:
      return state
  }
}

export default userReducer
