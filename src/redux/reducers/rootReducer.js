import {combineReducers} from 'redux'
import userReducer from './userReducer'
import { authenticationReducer } from '../../features/authentication/redux/reducers'

const reducer = combineReducers({
  authenticationReducer
})

export default reducer
