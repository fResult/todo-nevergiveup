import React from 'react'
import { Modal, Typography } from 'antd'
const { Title } = Typography

const ModalConfirmRemoveTodo = props => {
  const { visible, isRemoveLoading, onCancel, todo, onConfirmRemove } = props
  console.log('ModalConfirmRemoveTodo', todo)
  return (
    <>
      <Modal
        visible={visible}
        title={<Title level={3}>Remove Todo</Title>}
        okButtonProps={{ loading: isRemoveLoading, disabled: isRemoveLoading }}
        okText="Remove"
        okType="danger"
        onOk={() => onConfirmRemove(todo._id)}
        onCancel={onCancel}
      >
        Want delete {todo.title} ?
      </Modal>
    </>
  )
}

export default ModalConfirmRemoveTodo
