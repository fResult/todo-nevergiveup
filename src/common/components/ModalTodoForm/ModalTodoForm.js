import React from 'react'
import { Modal, Form, Input, Typography } from 'antd'
const { Title } = Typography

const ModalTodoForm = props => {
  const { visible, todo, onCancel, onCreateOrEditTodo, isEditLoading } = props
  const { title, description } = todo
  const [form] = Form.useForm()

  return (
    <>
      <Modal
        visible={visible}
        title={<Title style={{ textAlign: 'center' }}>{`${todo._id ? 'Edit' : 'Create'} Todo`}</Title>}
        okText={todo._id ? 'Edit' : 'Create'}
        okButtonProps={{
          loading: isEditLoading,
          disabled: isEditLoading,
          style: { background: !todo._id ? '#57A99A' : '#1E90FF', border: 'none' },
        }}
        onCancel={onCancel}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()
              onCreateOrEditTodo(values)
            })
            .catch(info => console.warn('Validate Failed:', info))
        }}
      >
        <Form form={form} name="todoForm" layout="vertical" size="large" initialValues={{ title, description }}>
          <Form.Item name="title" label="Title" rules={[{ required: true, message: 'Title cannot empty' }]}>
            <Input />
          </Form.Item>
          <Form.Item name="description" label="Description">
            <Input.TextArea />
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}

export default ModalTodoForm
