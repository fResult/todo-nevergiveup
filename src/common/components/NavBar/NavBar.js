import React from 'react'
import { Button } from 'antd'
import { useHistory } from 'react-router-dom'
import { LoadingOutlined } from '@ant-design/icons'

const NavBar = ({ logoutLoading }) => {

  const history = useHistory();
  // const isAuthenticated = useSelector(state => state.authenticationReducer.isAuthenticated)
  const isAuthenticated = localStorage.getItem('isAuthenticated')

  return (
    <>
      <div align="right">
        {!isAuthenticated ? (
          <Button onClick={() => history.push('/auth/login')}>Login</Button>
        ) : (
          <>
            {/*{username && <Button>{userInfo.username}</Button>}*/}
            <Button onClick={() => history.push('/auth/logout')} disabled={logoutLoading}>
              {!logoutLoading ? 'Logout' : <LoadingOutlined />}
            </Button>
          </>
        )}
      </div>
    </>
  )
}

export default NavBar
