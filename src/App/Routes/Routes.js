import React, { lazy, Suspense } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import Home from '../../pages/Home'
import PrivateRoute from '../../common/components/PrivateRoute'

const Authentication = lazy(() => import('../../features/authentication/Routes/'))
const Registration = lazy(() => import('../../features/registration/Routes'))
const Todos = lazy(() => import('../../pages/Todos'))

const routes = [
  { restricted: false, path: '/auth', Component: Authentication },
  { restricted: false, path: '/register', Component: Registration },
  { restricted: true, path: '/todos', Component: Todos },
  { restricted: false, path: '/', Component: Home },
]

const Routes = () => {
  return (
    <Suspense fallback={<div />}>
      <Switch>
        {routes.map(({ restricted, path, Component }) =>
          restricted ? (
            <PrivateRoute key={path} path={path} component={Component} />
          ) : (
            <Route key={path} path={path} component={Component} />
          ),
        )}
        <Redirect to={routes[routes.length - 1].path} />
      </Switch>
    </Suspense>
  )
}

export default Routes
