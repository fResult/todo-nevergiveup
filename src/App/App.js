import React from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import './App.css'
import Home from '../pages/Home'
import { Provider } from 'react-redux'
import store from '../redux/store'

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <div className="App">
            <Home />
          </div>
        </Switch>
      </Router>
    </Provider>
  )
}

export default App
