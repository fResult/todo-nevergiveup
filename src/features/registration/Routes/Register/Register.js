import React from 'react'
import { Button, Form, Input } from 'antd'
import { useHistory } from 'react-router-dom'
import { registerService } from '../../services/'
import { openRegisterFailedNotification } from '../../services/registerNotification'
import styles from './Register.module.css'

const layout = { labelCol: { xs: 24, sm: 8 }, wrapperCol: { xs: 24, sm: 16 } }
const tailLayout = { wrapperCol: { xs: { offset: 12, span: 24 }, sm: { offset: 0, span: 16 } } }

const Register = () => {
  const [form] = Form.useForm()
  const history = useHistory()
  const service = registerService(form)

  const handleRegister = async ({ user }) => {
    try {
      await service.register(user)
      history.push('/auth/login')
    } catch (err) {
      console.error('Error ❌', err.response.status, err.response.data)
      openRegisterFailedNotification(`Register Failed, ${err.response.data}`)
    }
  }

  const onFinishFailed = errorInfo => {
    console.warn('Failed:', errorInfo)
  }

  return (
    <>
      <div align="center">
        <Form {...layout}
              className={styles.RegisterForm}
              form={form}
              name="Register"
              onFinish={handleRegister}
              onFinishFailed={onFinishFailed}
        >
          <Form.Item
            name={['user', 'username']}
            label="Username"
            rules={[
              { required: true, message: 'Please input Username' },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={['user', 'password']}
            label="Password"
            rules={[
              {
                required: true,
                message: 'Please input your Password!',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name={['user', 'confirmPassword']}
            label="Confirm Password"
            dependencies={['user', 'password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (value && getFieldValue(['user', 'password']) === value) {
                    return Promise.resolve()
                  }
                  return Promise.reject('Password and Confirm Password do not match!')
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">Submit</Button>
            <Button type="secondary" onClick={() => history.push('/auth/login')}>Login</Button>
          </Form.Item>
        </Form>
      </div>
    </>
  )
}

export default Register
