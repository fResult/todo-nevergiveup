import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { LoadingOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from 'react-redux'
import { logoutSuccess } from '../../redux/actions'

const Logout = () => {
  const history = useHistory()
  const dispatch = useDispatch()

  useEffect(() => {
    handleLogout()
  }, [handleLogout])

  function handleLogout() {
    // localStorage.removeItem('isAuthenticated')
    // localStorage.removeItem('token')
    dispatch(logoutSuccess())
    setTimeout(() => history.push('/auth/login'), 3000)
  }

  return (
    <>
      <div style={{ fontSize: 128, position: 'fixed', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
        <LoadingOutlined />
      </div>
    </>
  )
}

export default Logout
