import React, { lazy, Suspense } from 'react'
import { Switch, Route, Redirect, useLocation, useRouteMatch } from 'react-router-dom'

const Login = lazy(() => import(('./Login')))
const Logout = lazy(() => import('./Logout'))

const routes = [
  { path: '/login', Component: Login },
  { path: '/logout', Component: Logout },
]

const Routes = () => {
  const location = useLocation()
  const match = useRouteMatch()
  return (
    <Suspense fallback={<div />}>
      <Switch location={location}>
        {routes.map(({ path, Component }) => (
          <Route key={path} exact={true} path={`${match.path}${path}`} component={Component} />
        ))}
        <Redirect to={`${match.path}${routes[0].path}`} />
      </Switch>
    </Suspense>
  )
}

export default Routes
