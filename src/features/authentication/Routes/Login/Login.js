import React from 'react'
import { Button, Checkbox, Form, Input } from 'antd'
import { useDispatch } from 'react-redux'
import { loginService } from '../../services'
import { useHistory } from 'react-router-dom'
import { openLoginFailedNotification } from '../../services/loginNotification'
import styles from './Login.module.css'
import { loginSuccess } from '../../redux/actions'

const layout = { labelCol: { xs: 24, sm: 8 }, wrapperCol: { xs: 24, sm: 16 } }
const tailLayout = { wrapperCol: { xs: { offset: 0, span: 24 }, sm: { offset: 0, span: 16 } } }

const Login = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  // const isAuthenticated = useSelector(state => state.authenticationReducer.isAuthenticated)
  // const isAuthenticated = JSON.parse(localStorage.getItem('userInfo'))
  // isAuthenticated && history.push('/daily')
  const handleLogin = async ({ username, password, remember }) => {
    try {
      const token = await loginService().login(username, password)
      dispatch(loginSuccess(token))
      history.push('/todos')
    } catch (err) {
      let errorMessage = ''
      if (err.response) {
        errorMessage = err.response.data.message
        console.error('Error ❌', err.response.status, errorMessage)
      } else {
        errorMessage = err.message
        console.error(err)
      }
      openLoginFailedNotification(errorMessage)
    }
  }

  const onFinishFailed = errorInfo => {
    errorInfo.errorFields.map(errorField => console.warn(errorField.errors[0]))
  }

  return (
    <>
      <div align="center">
        <Form
          {...layout}
          className={styles.FormLogin}
          name="Login"
          initialValues={{ remember: true, username: '', password: '' }}
          onFinish={handleLogin}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your Username!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your Password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Login
            </Button>
            <Button type="secondary" onClick={() => history.push('/register')}>
              Register
            </Button>
          </Form.Item>
        </Form>
      </div>
    </>
  )
}

export default Login
