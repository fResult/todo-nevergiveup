const logoutService = () => {
  return {
    logout: () => {
      localStorage.removeItem('userItem')
    }
  }
}

export default logoutService
