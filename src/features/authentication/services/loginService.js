import { openLoginSuccessNotification } from './loginNotification'
import axios from '../../../configs/api.service'

const loginService = () => {
  return {
    login: async (username, password) => {
      const { data: { token } } = await axios.post('users/auth', { username, password })
      openLoginSuccessNotification(`Username ${username} login successfully`)
      return token
    },
  }
}

export default loginService
