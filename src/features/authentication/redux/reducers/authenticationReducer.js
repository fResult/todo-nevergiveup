import { authenticationTypes } from '../types'

const initAuthState = {
  // TODO: when Cors error is fixed must change isAuthenticated from true to be false
  isAuthenticated: false,
  token: '',
}

const authenticationReducer = (state = initAuthState, action) => {
  switch (action.type) {
    case authenticationTypes.LOGIN_SUCCESS:
      return {
        isAuthenticated: action.isAuthenticated,
        token: action.token,
      }
    case authenticationTypes.LOGOUT_SUCCESS:
      return {
        isAuthenticated: false,
      }
    default:
      return state
  }
}

export default authenticationReducer
