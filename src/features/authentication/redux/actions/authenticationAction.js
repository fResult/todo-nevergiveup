import { authenticationTypes } from '../types'

// TODO: Remove hard code token
export const loginSuccess = (token) => {
  localStorage.setItem('token', token)
  localStorage.setItem('isAuthenticated', 'true')
  return {
    type: authenticationTypes.LOGIN_SUCCESS,
    isAuthenticated: true,
    token: token
  }
}

export const logoutSuccess = () => {
  localStorage.removeItem('isAuthenticated')
  localStorage.removeItem('token')
  return {
    type: authenticationTypes.LOGOUT_SUCCESS,
    isAuthenticated: false,
  }
}
