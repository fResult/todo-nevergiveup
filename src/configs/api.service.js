import axios from "axios";

axios.defaults.baseURL = 'https://candidate.neversitup.com/todo';

export default axios;
