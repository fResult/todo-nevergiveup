import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import axios from '../../configs/api.service'
import { Button, Card, Col, Modal, Row } from 'antd'
import styles from './Todos.module.css'
import ModalTodoForm from '../../common/components/ModalTodoForm'
import { PlusOutlined } from '@ant-design/icons'
import ModalConfirmRemoveTodo from '../../common/components/ModalConfirmRemoveTodo'

// const token = useSelector(state => state.authenticationReducer.token)
const token = localStorage.getItem('token')
const apiConfig = { headers: { Authorization: `Bearer ${token}` }, 'Content-Type': 'application/json' }

const Todos = () => {
  const [todos, setTodos] = useState([])
  const [todo, setTodo] = useState({})
  const [isEditLoading, setIsEditLoading] = useState(false)
  const [isRemoveLoading, setIsRemoveLoading] = useState(false)
  const [modalTodoVisible, setModalTodoVisible] = useState(false)
  const [modalConfirmRemoveVisible, setModalConfirmRemoveVisible] = useState(false)

  useEffect(async () => {
    await fetchTodos()
  }, [])

  const fetchTodos = async () => {
    try {
      const { data } = await axios.get('/todos', apiConfig)
      setTodos(data)
    } catch (err) {
      console.error(err)
      console.error(err.response.data)
    }
  }

  const handleSelectTodo = async todoId => {
    const { data } = await axios.get(`/todos/${todoId}`, apiConfig)
    setTodo(data)
    setModalTodoVisible(true)
  }

  const handleOpenCreateTodoModal = () => {
    setModalTodoVisible(true)
    setTodo({})
  }

  const handleCreateOrEditTodo = async values => {
    setIsEditLoading(true)

    if (todo._id) {
      await axios.put(
        `/todos/${todo._id}`,
        {
          _id: todo._id,
          title: values.title,
          description: values.description,
        },
        apiConfig,
      )
    } else {
      await axios.post(
        `/todos`,
        {
          title: values.title,
          description: values.description,
        },
        apiConfig,
      )
    }

    setIsEditLoading(false)
    setModalTodoVisible(false)
    await fetchTodos()
  }

  const handleRemoveTodo = async todoId => {
    const { data } = await axios.get(`/todos/${todoId}`, apiConfig)
    setTodo(data)
    setModalConfirmRemoveVisible(true)
  }

  const handleConfirmRemove = async todoId => {
    console.log('Hello', todoId)
    setIsRemoveLoading(true)

    try {
      await axios.delete(`/todos/${todoId}`, apiConfig)
    } catch (err) {
      console.error(err)
      console.error(err.response.data)
    }
    await fetchTodos()

    setModalConfirmRemoveVisible(false)
    setIsRemoveLoading(false)
  }

  return (
    <>
      <div>
        <Row>
          <Col span={6} />
          <Col span={12}>
            {todos.map(todo => (
              <Card
                className={styles.CardTodo}
                headStyle={{ textAlign: 'left', background: '#F9957F' }}
                bodyStyle={{ textAlign: 'left', background: '#C7B198' }}
                key={todo._id}
                title={todo.title}
                onClick={() => handleSelectTodo(todo._id)}
                extra={
                  <Button
                    type="danger"
                    shape="circle"
                    style={{ color: 'white', zIndex: 10 }}
                    onClick={async e => {
                      e.stopPropagation();
                      await handleRemoveTodo(todo._id)
                    }}
                  >
                    X
                  </Button>
                }
              >
                {todo.description}
              </Card>
            ))}
          </Col>
          <Col span={6} />
        </Row>

        <div className={styles.ContainerButtonCreate}>
          <Button type="primary" className={styles.ButtonCreate} onClick={handleOpenCreateTodoModal}>
            Create <PlusOutlined />
          </Button>
        </div>
      </div>

      {modalTodoVisible && (
        <ModalTodoForm
          visible={modalTodoVisible}
          todo={todo}
          isEditLoading={isEditLoading}
          onCancel={() => setModalTodoVisible(false)}
          onCreateOrEditTodo={handleCreateOrEditTodo}
        />
      )}

      {modalConfirmRemoveVisible && (
        <ModalConfirmRemoveTodo
          visible={modalConfirmRemoveVisible}
          todo={todo}
          isRemoveLoading={isRemoveLoading}
          onConfirmRemoveTodo={handleConfirmRemove}
          onCancel={() => setModalConfirmRemoveVisible(false)}
          onConfirmRemove={handleConfirmRemove}
        />
      )}
    </>
  )
}

export default Todos
